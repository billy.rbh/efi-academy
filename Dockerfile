######stage build

FROM maven:3.8.6-jdk-8-slim as build
RUN mkdir /app 
COPY . /app
WORKDIR /app
RUN mvn clean install package



######stage RUN
FROM tomcat
COPY --from=build /app/webapp/target/webapp.war /var/local/tomcat/webapps
